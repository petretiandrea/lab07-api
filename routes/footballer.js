var express = require('express');
var router = express.Router();
var mongoose = require("mongoose")
var Footballer = require("../model/Footballer");

var uriString = 'mongodb://heroku_ztkpjkpk:iifomiteo2kf59g9kvvvv8holp@ds135036.mlab.com:35036/heroku_ztkpjkpk';
//var uriString = 'mongodb://localhost/footballer';

mongoose.connect(uriString, { useNewUrlParser: true }, function (err, res) {
  if (err) {
  console.log ('ERROR connecting to: ' + uriString + '. ' + err);
  } else {
  console.log ('Succeeded connected to: ' + uriString);
  }
});

/* Read API */
router.get('/:space/footballers', function(req, res, next) {
  Footballer.find({space: req.params.space}, function(err, footballers) {
    if(!err) {
      sendResponseMessage(res, 200, footballers);
    }
  });
});

router.get('/:space/footballers/:id', function(req, res, next) {
  Footballer.findOne({id: req.params.id, space: req.params.space}, function(err, footballer) {
    if(!err && footballer != undefined) {
      sendResponseMessage(res, 200, footballer);
    } else {
      sendResponseMessage(res, 404, "Not Found");
    }
  });
});

/* Modify API */
router.post('/:space/footballers', function(req, res, next) {
  req.body.space = req.params.space;
  var toInsert = new Footballer(req.body);
  console.log(toInsert);
  var error = toInsert.validateSync();
  if(error) {
    sendResponseMessage(res, 400, "Bad Request");
  } else {
    toInsert.save(function(err, footballer) {
      if(!err) { sendResponseMessage(res, 201, footballer); } 
      else {
        if(err.code == 11000) {
          sendResponseMessage(res, 409, "Conflict"); 
        } else {
          sendResponseMessage(res, 500, "Internal Error"); 
        }
      }
    });
  }
});

router.put('/:space/footballers/:id', function(req, res, next) {
  Footballer.findOne({id: req.params.id, space: req.params.space}, function(err, footballer) {
      if(!err && footballer != undefined) {
        req.body.space = req.params.space;
        var update = new Footballer(req.body);
        if(!update.validateSync()) {
          footballer.name = update.name;
          footballer.surname = update.surname;
          footballer.nGoals = update.nGoals;
          footballer.image = update.image;
          footballer.modified = new Date();
          footballer.save((err, footballer) => {if(!err) { sendResponseMessage(res, 200, footballer); } else { sendResponseMessage(res, 500, "Internal Error"); } });
        } else {
          sendResponseMessage(res, 400, "Bad request");
        }
      } else {
        sendResponseMessage(res, 404, "Not Found");
      }
  });
});

router.delete('/:space/footballers/:id', function(req, res, next) {
  Footballer.deleteOne({id: req.params.id, space: req.params.space}, (err) => {
    if(!err) { sendResponseMessage(res, 200, ""); } else { sendResponseMessage(res, 404, "Not Found"); }
  });
});

module.exports = router;

/* Funzione per inviare una risposta HTTP */
function sendResponseMessage(res, httpCode, message)
{
	console.log("HTTP-Status: " + httpCode +  " Message: " + message);
	return res.status(httpCode).json(message);
}
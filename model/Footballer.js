var mongoose = require("mongoose");
const AutoIncrement = require('mongoose-sequence')(mongoose);

function isEmpty(value) {
    return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
  }

var FootballerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return !isEmpty(v);
            },
            message: props => `${props.value} is not a valid name`
        }
    },
    surname: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return !isEmpty(v);
            },
            message: props => `${props.value} is not a valid surname`
        }
    },
    nGoals: {
        type: Number,
        default: 0,
        validate: {
            validator: function(v) {
                return v >= 0;
            },
            message: props => `${props.value} is not a valid goal number`
        }
    },
    image: {
        type: String,
        default: ""
    },
    space: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return !isEmpty(v);
            },
            message: props => `${props.value} is not a valid space`
        }
    }
});

FootballerSchema.index({ name: 1, surname: 1, space: 1 }, { unique: true });
FootballerSchema.plugin(AutoIncrement, {inc_field: 'id'});
FootballerSchema.options.toJSON = {
    transform: function(doc, ret, options) {
        delete ret._id;
        delete ret.__v;
        delete ret.space;
        return ret;
    }
};

var Footballer = mongoose.model('Footballer', FootballerSchema);

module.exports = Footballer